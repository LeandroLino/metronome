let horaFinal = new Date("december 31, 2027").getTime();
let anos, meses, dias, horas, minutos, segundos

setInterval(function() {

    let horaAtual = new Date().getTime();
    let segundosHoraFinal = (horaFinal - horaAtual) / 1000;

    anos = parseInt(segundosHoraFinal / 31536000);
    segundosHoraFinal = segundosHoraFinal % 31536000

    meses = parseInt(segundosHoraFinal / 2628003);
    segundosHoraFinal = segundosHoraFinal % 2628003

    dias = parseInt(segundosHoraFinal / 86400);
    segundosHoraFinal = segundosHoraFinal % 86400

    horas = parseInt(segundosHoraFinal / 3600);
    segundosHoraFinal = segundosHoraFinal % 3600

    minutos = parseInt(segundosHoraFinal / 60);
    segundos = parseInt(segundosHoraFinal % 60);

    minutos = zero(minutos);
    segundos = zero(segundos);
    horas = zero(horas)
    meses = zero(meses)

    document.getElementById('ano').innerHTML = anos + "."
    document.getElementById('mes').innerHTML = meses + "."
    document.getElementById('dia').innerHTML = dias + " ";
    document.getElementById('hora').innerHTML = horas + ":";
    document.getElementById('minutos').innerHTML = minutos + ":";
    document.getElementById('segundos').innerHTML = segundos;
    //setar cor de acordo com o valor
    if (anos > 10) {
        ano.style.color = "red"
    }
}, 100)

function zero(x) {
    if (x < 10) {
        x = '0' + x;
    }
    return x;
}